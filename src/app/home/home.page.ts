import { Component, OnInit, NgZone } from "@angular/core";
import {
  Plugins,
  PushNotification,
  PushNotificationToken,
} from "@capacitor/core";

const { PushNotifications } = Plugins;
import { Platform } from "@ionic/angular";
import { FCM } from "@capacitor-community/fcm";
const fcm = new FCM();

@Component({
  selector: "app-home",
  templateUrl: "home.page.html",
  styleUrls: ["home.page.scss"],
})
export class HomePage implements OnInit {
  session: any;

  notifications: PushNotification[] = [];
  //
  // move to fcm demo
  topicName = "super-awesome-topic";
  remoteToken: any;

  constructor(private platform: Platform, private zone: NgZone) {}

  ngOnInit() {
    PushNotifications.addListener("registration", (data) => {
      // alert(JSON.stringify(data));
      console.log(data);
    });
    PushNotifications.register().then(() => alert(`registered for push`));
    PushNotifications.addListener(
      "pushNotificationReceived",
      (notification: PushNotification) => {
        console.log("notification " + JSON.stringify(notification));
        this.zone.run(() => {
          this.notifications.push(notification);
        });
      }
    );
  }

  //
  // move to fcm demo
  subscribeTo() {
    PushNotifications.register()
      .then((_) => {
        fcm
          .subscribeTo({ topic: this.topicName })
          .then((r) => alert(`subscribed to topic ${this.topicName}`))
          .catch((err) => console.log(err));
      })
      .catch((err) => alert(JSON.stringify(err)));
  }

  unsubscribeFrom() {
    fcm
      .unsubscribeFrom({ topic: "test" })
      .then((r) => alert(`unsubscribed from topic ${this.topicName}`))
      .catch((err) => console.log(err));
    if (this.platform.is("android")) fcm.deleteInstance();
  }

  getToken() {
    // Push notification settings...
    PushNotifications.register().then(() => console.log(`registered for push`));
    // Select platform
    if (this.platform.is("android")) {
      PushNotifications.addListener(
        "registration",
        (token: PushNotificationToken) => {
          console.log("Push registration success, token: " + token.value);
          alert("Push registration success, token: " + token.value);
          this.remoteToken = token.value;
        }
      );
    } else {
      PushNotifications.addListener("registration", () => {
        console.log("Registered...");
      });
      // Get FCM token instead the APN one returned by Capacitor
      fcm
        .getToken()
        .then((r) => {
          console.log(`Token ${r.token}`);
          alert(`Token ${r.token}`);
          this.remoteToken = r.token;
        })
        .catch((err) => console.log(err));
    }

    PushNotifications.addListener(
      "pushNotificationReceived",
      (notification: PushNotification) => {
        console.log(notification);
        alert(notification);
      }
    );
  }
}
